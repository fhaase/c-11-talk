#### **nullptr** constant

+++
#### **nullptr** constant

There is **NULL**, isn't it ?

```c++
void f(char const *ptr);
void f(int v);

f(NULL);  // which function will be called?
``` 

* Cpp98 - macro definition: **NULL** 
* integer type that evaluates to zero
* In C, the NULL may be void*, but not in C++. 
* Cpp11 - keyword: **nullptr** 

+++
#### **nullptr** 

1. Own type **std::nullptr_t**
2. implicitly convertible and comparable to any pointer or pointer-to-member
3. Not implicitly convertible or comparable to integral types, except for bool. 

+++
#### **nullptr** constant

```c++
void f(char const *ptr);
void f(int v);

f(nullptr); // which function will be called?

char *pc = nullptr;     // OK
int  *pi = nullptr;     // OK
bool   b = nullptr;     // OK. b is false.
int    i = nullptr;     // error
```

**How and where should you use nullptr?**

-> Whenever you would have otherwise used **NULL** in the past.

```c++
#define NULL  nullptr
```