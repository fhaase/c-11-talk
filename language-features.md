#### C++11 Language features

**__cplusplus** 

alignments 

attributes

atomic operations

auto (type deduction from initializer)

C99 features

**enum class (scoped and strongly typed enums)**

+++

copying and rethrowing exceptions  

constexpr ( constant expressions )

decltype

**control of defaults: default and delete**

control of defaults: move and copy

delegating constructors

Dynamic Initialization and Destruction with Concurrency 

exception propagation (preventing it; noexcept)

+++

explicit conversion operators

**extended integer types**

extern templates

suffix return type syntax (extended function declaration syntax)

**in-class member initializers**

inherited constructors

**initializer lists (uniform and general initialization)**

Inline namespace

+++

**lambdas**

local classes as template arguments

long long integers (at least 64 bits)

memory model

move semantics

narrowing (how to prevent it)

**null pointer (nullptr)**

**override controls**

+++

PODs (generalized)

**range-for statement**

raw string literals

**right-angle brackets**

Simple SFINAE rule

**static (compile-time) assertions (static_assert)**

template alias

thread-local storage (thread_local)

+++

unicode characters

**Uniform initialization syntax and semantics**

unions (generalized)

user-defined literals

variadic templates 