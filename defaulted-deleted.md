#### Explicitly defaulted and deleted special member functions

+++
#### Explicitly defaulted and deleted special member functions

What are the special member functions ?

- **Constructors**
- **Destructors**
- **Copy-constructors**
- **Copy-assignment operators**
- Move-constructors
- Move-assignment operators

+++
#### Cpp98 - defaulted special member function

```c++
class Class_C
{
public:
  Class_C(int a){};
};

Class_C obj; // Fails to compile
```

- Default constructor not generated 

```c++
class Class_C
{
public:
  Class_C(int a){};
  Class_C(){};
};

Class_C obj; // Now compiles
```

+++
#### Cpp11 - defaulted special member function

Solution:
- Explicitly default the constructor

```c++
class Class_C
{
public:
  Class_C(int a){};
  Class_C() = default;
};

Class_C obj; // Calls explicitly generated default constructor
```

+++
#### Cpp98 - deleted special member function

Solution: 
- Make functions private
- Do not provide implementation

```c++
class Class_C
{
private:

  Class_C( const Class_C& other){};
  
  Class_C& operator=( const Class_C& other){};
};
```

+++
#### Cpp11 - deleted special member function

Solution: 
- Explicitly delete the function

```c++
class Class_C
{
public:

  Class_C( const Class_C& other) = delete;
  
  Class_C& operator=( const Class_C& other) = delete;
};
```
