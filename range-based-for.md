#### Range-based for loop 

+++?code=cpp98/range-based-for.cpp&lang=c++&title="Cpp98 // range-based for loop"

+++
#### Range-based for loop 

* Before: BOOST_FOREACH
* Now: range-based-for
* Works with const/ref specifiers too

+++?code=cpp11/range-based-for.cpp&lang=c++&title="Cpp11 // range-based for loop"

