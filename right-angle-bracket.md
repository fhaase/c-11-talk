#### right angle bracket **>>**


+++
#### right angle bracket **>>**

```c++
// C++98
std::vector<std::unique_ptr<int> > myInts;

// C++11
std::vector<std::unique_ptr<int>> myInts;
```