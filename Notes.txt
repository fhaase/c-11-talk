C++11 topics !




#### Use cases for language features


####TODO:


Move semantics -> One or two slides
Exception specifications are deprecated, now we have noexcept ! -> Slides

Lambda -> Code

Initializer lists and uniform initialization -> Code
Object construction improvements (Curly Braces) -> Code

User defined literals -> Leave out
decltype -> Leave out
constexpr -> Leave out

####STARTED:

nullptr -> Slide
Right angle bracket -> Slide

Range-based for loop -> Code
auto -> Code

Enhanced enums -> Code

Explicitly defaulted and deleted special member functions -> Code
Explicit overrides and final -> Code
Delegating constructors -> Code
Data member initialization (any non-static data member can be initialized in its declaration) -> Code

####DONE:

####Not completely clear:

constexpr

