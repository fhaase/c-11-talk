#### in-class member initializers

+++
#### Cpp98 in-class member initializers

* only static const members of integral types can be initialized in-class, and the initializer has to be a constant expression.
* ensures initialization at compile-time

```c++
int var = 7;

class Class_C {
  static const int    m1 = 7;     // ok
  const int           m2 = 7;     // error: not static
  static int          m3 = 7;     // error: not const
  static const int    m4 = var;   // error: initializer not constant expression
  static const string m5 = "odd"; // error: not integral type
};
```

* for everything else: look into .cpp file and constructor initializer list**s**

+++
#### Cpp11 in-class member initializers

* allow non-static data member to be initialized where it is declared (in its class)
* If a member is initialized by both an in-class initializer and a constructor, only the constructor's initialization is done

```c++
class Class_C {
public:
  int a = 7;
};

class Class_C
{
public:
  A() {}
  A(int a_val) : a(a_val) {}
  A(D d) : b(d) {}
  int a = 7;
  int b = 5;
}
``` 