#### Strongly typed enumerations

+++?code=cpp98/enums.cpp&lang=c++&title="Cpp98 // **enum**"


#### Problems with enums in Cpp98

- Enumerators leak into the surrounding scopes |
- Implicit conversions |

+++
#### Strongly typed enumerations

* new concept: **enum class**
* also called *scoped enumerations*
* properties:
 1. enumerator names are local to the enum 
 2. values do not implicitly convert to other types

+++?code=cpp11/enums.cpp&lang=c++&title="Cpp11 // **enum <mark>class</mark> **"