// Find the bug

std::vector<int> fibVec = { 1, 1, 2, 3, 5, 8, 13};

std::vector<int>::iterator iVec;
for ( ; iVec != fibVec.end(); ++iVec  )
{
  std::size_t position = iVec - fibVec.begin();
  if ( *iVec == 5 )
  {
    std::cout << "Value 5 is at position " << position << std::endl;
  }
}

std::vector<int> fibVec = { 1, 1, 2, 3, 5, 8, 13};

std:vector<int>::const_iterator iVec = fibVec.begin();
for ( ; iVec != fibVec.end(); ++iVec  )
{
  std::vector<int>::difference_type position = iVec - fibVec.begin();
  if ( *iVec == 5 )
  {
    std::cout << "Value 5 is at position " << position << std::endl;
  }
}

{
  // Iterate over map, increment each age by one, print name with youngest age
  std::string* youngestName = 0;
  int youngestAge = std::numeric_limits::max<int>();

  std::map< std::string, int >::const_iterator iMap;
  for ( iMap = ageMap.begin(); iMap != ageMap.end() ; ++iMap )
  {
    // Get element
    std::pair< std::string, int>& element = *iMap;
    // Increment age
    element.second += 1;  
    // Compare
    if ( youngestAge > element.second )
    {
      youngestAge = element.second;
      youngestName = &element.first;
    }
  }
  std::cout << *youngestName << " is the youngest person in your map !" << std::endl;
}

#elif // C++11

{
std::string testString = "This is a test";
 
auto autoString = testString;            // Copy, type is std::string

const auto autoStringConst = testString; // Copy, type is const std::string

auto& autoStringRef = testString;        // No copy, type is std::string& 

const autoStringConstRef& = testString;  // No copy, type is const std::string&

}

{
  // Iterate over vec, find element with value 5 and print position in vector
  auto iVec = fibVec.begin();
  for ( ; iVec != fibVec.end() ; ++iVec )
  {
    auto position = iVec - fibVec.begin();
    if ( *iVec == 5 )
    {
      std::cout << "Value 5 is at position " << position << std::endl
    }
  }
}

{
  // Iterate over map, increment each age by one, print name with youngest age
  std::string* youngestName = nullptr;
  const auto youngestAge = std::numeric_limits::max<int>();

  for ( auto& element : ageMap )
  {
    element.second += 1;  
    // Compare
    if ( youngestAge > element.second )
    {
      youngestAge = element.second;
      youngestName = &element.first;
    }
  }

  std::cout << *youngestName << " is the youngest person in your map !" << std::endl;
}

#endif

