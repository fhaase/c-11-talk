std::vector<int> fibVec = { 1, 1, 2, 3, 5, 8, 13};

std:vector<int>::const_iterator iVec = fibVec.begin();
for ( ; iVec != fibVec.end(); ++iVec  )
{
  std::vector<int>::difference_type position = iVec - fibVec.begin();
  if ( *iVec == 5 )
  {
    std::cout << "Value 5 is at position " << position << std::endl;
  }
}