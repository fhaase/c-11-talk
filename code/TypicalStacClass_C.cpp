//// Typical STAC class example CPP

#include "TypicalStacClass_C.h"

#ifdef // C++98

// One parameter constructor
TypicalStacClass_C::TypicalStacClass_C( const int aLogFb ) : 
  mLogFb( aLogFb ),
  mTimeout( boost::posix_time::seconds(5) )
{
}

// Two parameter constructor
TypicalStacClass_C::TypicalStacClass_C( const int aLogFb, const boost::posix_time::time_duration& aTimeout ) :
  mLogFb( aLogFb ),
  mTimeout( aTimeout)
{
}

// Default destructor
TypicalStacClass_C::~TypicalStacClass_C()
{
}

// Does this override a base class function ?
void TypicalStacClass_C::OverridingMemberFunc()
{
}

// Do derived classes override this method ?
void TypicalStacClass_C::FinalMemberFunc()
{
}

#elif // C++11

// One parameter constructor
TypicalStacClass_C::TypicalStacClass_C( const int aLogFb ) : 
  mLogFb( aLogFb ),
  //mTimeout( std::chrono::seconds(5) ) //No need to provide this here again !
{
}

// Two parameter constructor
TypicalStacClass_C::TypicalStacClass_C( const int aLogFb, const std::chrono::duration& aTimeout ) :
  TypicalStacClass_C( aLogFb ),
  mTimeout( aTimeout)
{
}

// Must override a member function of the base class, or else compiler error
void TypicalStacClass_C::OverridingMemberFunc()
{
}

// Can not be overridden by a derived class, or else compiler error
void TypicalStacClass_C::FinalMemberFunc()
{
}

#endif