#include <vector>
#include <map>
#include <string>
#include <iostream>

int main (void)
{
 struct X 
  {
    int mA;
    int mB;
    
    void print(){ std::cout << "X : " << mA << ", " << mB << std::endl; }
  };
  
  struct Y
  {
    Y(int a, int b){ std::cout << "Y( " << a << ", " << b << ")" << std::endl; }
  };
  
  struct Z
  {
    int mA;
    int mB;
    
    Z(int a, int b) : mA(b), mB(a) {}
    
    void print(){ std::cout << "Z : " << mA << ", " << mB << std::endl; }
  };

  X x1 = X{1,2}; x1.print();
  X x2 = {1,2}; x2.print();
  X x3{1,2}; x3.print();
  
  Y y1 = Y{1,2};
  Y y2 = {1,2}; 
  Y y3{1,2}; 
  
  Z z1 = Z{1,2}; z1.print();
  Z z2 = {1,2}; z2.print();
  Z z3{1,2}; z3.print();
  
}