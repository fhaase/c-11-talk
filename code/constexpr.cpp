//// constexpr array initialization

const int NUM_ELEMENTS = 10;

#ifdef // C++98

// array for all elements
const int[NUM_ELEMENTS] fibArray = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34 };

// array for all pairs of elements
const int[NUM_ELEMENTS * NUM_ELEMENTS] pairsArray; // Error, does not compile

#elif // C++11

////> A constexpr specifier used in an object declaration implies const

// array for all elements
constexpr std::array<int,NUM_ELEMENTS> fibArray = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34 };

// array for all pairs of elements
constexpr int Pow2( int aBase )
{
  return aBase*aBase;
}

constexpr std::array<int, Pow2( NUM_ELEMENTS ) > pairsArray;

#endif