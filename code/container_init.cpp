//// Container initialization

#ifdef // C++98

// Vector initialization
std::vector<int> fibVec;

fibVec.push_back(1);
fibVec.push_back(1);
fibVec.push_back(2);
fibVec.push_back(3);
fibVec.push_back(5);
fibVec.push_back(8);
fibVec.push_back(13);

// Map initialization
std::map<std::string, int> ageMap;

ageMap.insert( std::pair<std::string, int>( "Max",  33 ) );
ageMap.insert( std::pair<std::string, int>( "Fred", 42 ) );
ageMap.insert( std::pair<std::string, int>( "Sam",  51 ) );

#elif // C++11

// Vector initialization
std::vector<int> fibVec = { 1, 1, 2, 3, 5, 8, 13};

// Map initialization
std::map<std::string, int> ageMap = { { "Max", 33}, {"Fred", 42 }, {"Sam", 51} };

//// New in C++11

// Emplacement
ageMap.emplace( "Max", 33 ); // Constructed directly in the container, no extra copies necessary

// Store non-copyable types without abusing shared_ptrs 
std::vector<NoCopies_C> noCopies;

noCopies.emplace_back( 3 ); // Constructed in-place, vector owns the object

#endif
