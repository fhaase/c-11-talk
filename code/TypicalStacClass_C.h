//// Typical STAC class example HEADER

class Base_C
{
}

#ifndef // C++98

class TypicalStacClass_C : public Base_C
{
public:

  // One parameter constructor
  TypicalStacClass_C( const int aLogFb);
  
  // Two parameter constructor
  TypicalStacClass_C( const int aLogFb, const boost::posix_time::time_duration& aTimeout);

  // No default constructor implicitly generated

  // Default destructor
  ~TypicalStacClass_C();

  // Does this override a base class function ?
  void OverridingMemberFunc();

  // Do derived classes override this method ?
  void FinalMemberFunc();

private:

  // Deleted default copy-constructor, or is it ?
  TypicalStacClass_C( const TypicalStacClass_C& );

  // Deleted default copy-assign, or is it ?
  TypicalStacClass_C& operator=( const TypicalStacClass_C& );
  
  const int mLogFb;
  boost::date_time::posix_time::time_duration mTimeout; // Must not forget to set this in constructor
  
}

#else // C++11

//// Explicitly defaulted and deleted special member functions
//// Explicit overrides and final
//// Delegating constructors
//// Data Member initialization

class TypicalStacClass_C : public Base_C
{
public:

  // One parameter constructor
  TypicalStacClass_C( const int aLogFb);
  
  // Two parameter constructor
  TypicalStacClass_C( const int aLogFb, const std::chrono::duration& aTimeout);
  
  // Explicitly deleted default constructor ( would still not be generated implicitly ! )
  TypicalStacClass_C() = delete;
  
  // Forcing a default destructor to be generated by the compiler. 
  ~TypicalStacClass_C() = default;

  // Explicitly deleted copy-constructor
  TypicalStacClass_C( const TypicalStacClass_C& ) = delete;

  // Explicitly deleted copy-assign
  TypicalStacClass_C& operator=( const TypicalStacClass_C& ) = delete;
  
  // Must override a member function of the base class, or else compiler error
  void MemberFuncOverride() override;
  
  // Can not be overridden by a derived class, or else compiler error
  void FinalMemberFunc() final;
  
private:

  const int             mLogFb;
  std::chrono::duration mTimeout = std::chrono::seconds(5);

}

#endif