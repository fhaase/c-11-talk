# C++ 11
## Introduction
#### for STAC 2.3

---
#### C++ Timeline [(Source)](https://isocpp.org/std/status)

<img src="media/timeline.png" width="75%">

---
#### Design Goals of C++11 [(Source)](http://www.stroustrup.com/C++11FAQ.html#specific-aims)

- Maintain stability and compatibility 
- Prefer libraries to language extensions 
- Prefer generality to specialization 
- Support both experts and novices 
- Increase type safety 
- Improve performance and ability to work directly with hardware 
- Fit into the real world 


Note: 
-- don't break old code, and if you absolutely must, don't break it quietly.
-- an ideal at which the committee wasn't all that successful; too many people in the committee and elsewhere prefer "real language features."  
-- focus on improving the abstraction mechanisms (classes, templates, etc.). 
-- novices can be helped by better libraries and through more general rules; experts need general and efficient features. 
-- primarily though facilities that allow programmers to avoid type-unsafe features. 
-- make C++ even better for embedded systems programming and high-performance computation. 
-- consider tool chains, implementation cost, transition problems, ABI issues, teaching and learning, etc. 

---?include=language-features.md

---
#### Goals of this talk

* Cover **many** of the **important** features
* Only language features, no std::library additions
* Code examples ( find the bugs )
* Ask questions at the end of each feature !

---?include=nullptr.md

---?include=enums.md

---?include=uniform-initialization.md

---?include=range-based-for.md

---?include=auto.md

---?include=exceptions.md

---?include=right-angle-bracket.md

---
## Class design

---?include=defaulted-deleted.md

---?include=override-final.md

---?include=delegating-constructors.md

---?include=member-init.md

---
#### important std::library additions

* regular expressions
* date and time
* smart pointers
* atomic operations
* improved containers

---
## Questions ?
#### Resources / Links

[Wikipedia](https://en.wikipedia.org/wiki/C%2B%2B11)

[Bjarne Stroustrup's C++11 FAQ ](http://www.stroustrup.com/C++11FAQ.html)

[cppreference.com](http://en.cppreference.com/w/)

---
#### Teaser : Move semantics

+++
#### Teaser : Move semantics

* rvalue references ( temporary objects )
* move (steal) the data instead of copying

```c++
class A
{
  A() = default;                        // Constructor
  ~A() = default;                       // Destructor
  A( const A& a) = default;             // Copy-Constructor
  A& operator=( const A& a)  = default; // Copy-Assignment operator
  A( A&& a ) = default;                 // Move constructor
  A& operator=( A&& a) = default        // Move-assignment operator
}
```

---
#### Teaser : lambdas

+++
#### Teaser : lambdas 

* Function that you can write inline in your code
* often used as input to another function
* able to capture variables

```c++
std::vector<int> v = { 1, 2, 3, 4, 4, 3, 7, 8, 9, 10 };

int num_items1 = std::count_if(v.begin(), v.end(), [](int i) {return i % 3 == 0;});

std::cout << "number divisible by three: " << num_items1 << '\n';
```

---
#### Teaser : **constexpr**

+++
#### Teaser : **constexpr**

* define compile time initialized constants
* using function calls

```c++
constexpr int factorial(int n)
{
    return n <= 1? 1 : (n * factorial(n - 1));
}

constexpr int x = factorial(4);   
```
