#### Exceptions

+++
#### Cpp98 - Exception specifications

```c++
int Foo() throw();    // will throw nothing

int Bar() throw(A,B); // can only throw A or B
```

* Specifies what exceptions a function might throw
* throw sth. else -> std::unexpected -> std::terminate
* This is deprecated in C++11, removed in C++17

+++
#### Cpp11 - **noexcept**

* If a function cannot throw an exception, that function can be declared **noexcept**.

```c++
void foo() noexcept(true) {}
void bar() noexcept(false) {}
void baz() noexcept {}  // noexcept is the same as noexcept(true)
```

* If a function declared noexcept throws (so that the exception tries to escape, the noexcept function) the program is terminated

* A generated destructor is implicitly noexcept