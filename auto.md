#### auto

+++
#### Spot the bug !
<img src="media/bug/bug2.jpg" width="75%">

+++
#### Cpp98 // no auto - bug 1

```c++
std::vector<int> fibVec = { 1, 1, 2, 3, 5, 8, 13};

// Iterate over fibonacci numbers, find position of value 5
std::vector<int>::iterator iVec;
for ( ; iVec != fibVec.end(); ++iVec  )
{
  std::size_t position = iVec - fibVec.begin();
  if ( *iVec == 5 )
  {
    std::cout << "Value 5 is at position " << position << std::endl;
  }
}
```

+++
#### Cpp98 // no auto - bug 1 - solution

```c++
std::vector<int> fibVec = { 1, 1, 2, 3, 5, 8, 13}; 

// Iterate over fibonacci numbers, find position of value 5
std:vector<int>::const_iterator iVec = fibVec.begin();
for ( ; iVec != fibVec.end(); ++iVec  )
{
  std::vector<int>::difference_type position = iVec - fibVec.begin();
  if ( *iVec == 5 )
  {
    std::cout << "Value 5 is at position " << position << std::endl;
  }
}
```

+++
#### Spot the bug !
<img src="media/bug/bug3.jpg" width="75%">

+++
#### Cpp98 // no auto - bug 2

```c++
std::map<std::string, int> intMap = { { "Max", 33}, {"Fred", 42}, {"Sam", 51} };

// Iterate over map, find person with age 42
const std::string* personOfInterest = nullptr;

std::map<std::string, int>::const_iterator iMap = intMap.begin();
for ( ; iMap != intMap.end() ; ++iMap )
{
  const std::pair<std::string,int> element = *iMap;
  if ( element.second == 42 )
  {
    personOfInterest = &element.first;
  }
}

if ( personOfInterest )
{
  std::cout << *personOfInterest << " is 42 years old !" << std::endl;
}
```

+++
#### Cpp98 // no auto - bug 2 - solution

```c++
std::map<std::string, int> intMap = { { "Max", 33}, {"Fred", 42}, {"Sam", 51} };

// Iterate over map, find person with age 42
const std::string* personOfInterest = nullptr;

std::map<std::string, int>::const_iterator iMap = intMap.begin();
for ( ; iMap != intMap.end() ; ++iMap )
{
  const std::pair<const std::string,int> element = *iMap;
  if ( element.second == 42 )
  {
    personOfInterest = &element.first;
  }
}

if ( personOfInterest )
{
  std::cout << *personOfInterest << " is 42 years old !" << std::endl;
}
```

+++
#### What were the problems ?

- Unitialized variables 
- Wordy declarations 
- Implicit conversions 

+++
#### Cpp11 - auto 

let the compiler deduce the type of a variable from rhs

-> No unitialized variables

-> Less typing

-> No implicit conversions

+++
#### Cpp11 - auto

```c++
// Iterate over fibonacci numbers, find position of value 5

std::vector<int> fibVec = { 1, 1, 2, 3, 5, 8, 13};

auto iVec = fibVec.begin(); // std::vector<int>::iterator
for ( ; iVec != fibVec.end(); ++iVec  )
{
  auto position = iVec - fibVec.begin(); // std::vector<int>::difference_type
  if ( *iVec == 5 )
  {
    std::cout << "Value 5 is at position" << position << std::endl;
  }
}
```

+++
#### Cpp11 - auto 

```c++
std::map<std::string, int> intMap = { { "Max", 33}, {"Fred", 42}, {"Sam", 51} };

// Iterate over map, find person with age 42
const std::string* personOfInterest = nullptr;

auto iMap = intMap.cbegin();
for ( ; iMap != intMap.end() ; ++iMap )
{
  auto element = *iMap;
  if ( element.second == 42 )
  {
    personOfInterest = &element.first;
  }
}

if ( personOfInterest )
{
  std::cout << *personOfInterest << " is 42 years old !" << std::endl;
}
```

+++
#### Conclusion

* One of the more controversial features
* Watch out for initializer lists
* Scott Meyers, Andrei Alexandrescu and Herb Sutter: 
> Almost always use auto (AAA)
* You can find more on this [here](https://herbsutter.com/2013/08/12/gotw-94-solution-aaa-style-almost-always-auto/)