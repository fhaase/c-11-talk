#### Uniform initialization

+++
#### Cpp98 - Container initialization

```c++
// array initialization
int arr[] = { 1,2,3,4,5 };

// Vector initialization
std::vector<int> fibVec;
fibVec.push_back(1); fibVec.push_back(1);
fibVec.push_back(2); fibVec.push_back(3);
fibVec.push_back(5); fibVec.push_back(8);

// Map initialization
std::map<std::string, int> ageMap;
ageMap["Max"] = 33;
ageMap["Fred"] = 42;
ageMap["Sam"] = 51;

// Set initialization
std::set<int> integerSet;
integerSet.insert(3); 
integerSet.insert(4); 
integerSet.insert(1);
```

+++
#### Cpp98 - Container initialization

Problems:

* Very verbose
* Too many different ways to achive the same thing

+++
#### Cpp11 - Initializer lists

```c++
// array initialization
int arr[] = { 1,2,3,4,5 };

// Vector initialization
std::vector<int> fibVec = { 1, 1, 2, 3, 5, 8, 13};

// Map initialization
std::map<std::string, int> ageMap = { { "Max", 33}, {"Fred", 42 }, {"Sam", 51} };

// Set initialization
std::set<int> integerSet = { 3, 4, 1};
```

+++
#### Cpp11 - Uniform initialization

Uniform Initialization expands on the Initializer List syntax
* works on any object 
  * aggregate + non-aggregate classes
  * arrays 
  * STL/custom collection classes
  * PODs

+++
#### Cpp11 Uniform initialization

```c++
char        c {}; // c = 0
int         i {}; // i = 0
double      d {}; // d = 0.0
bool        b {}; // b = false
std::string s {}; // s = ""
int*        p {}; // p = nullptr

char        c {'x'};   // c = 'x'
int         i {99};    // i = 99
double      d {1.23};  // d = 1.23
bool        b {true};  // b = true
std::string s {"cat"}; // s = "cat"
int*        p {&i};    // p = &i
    
int i {3};    // OK
int j {3.0};  // Error!
int k {3.14}; // Error!
```

+++
#### Cpp11 - Uniform initialization

* Allow {}-initializer lists for all initialization: 

```c++
struct X
{
  int a;
  int b;
};

X x1 = X{1,2}; // these 3 statements are equal
X x2 = {1,2}; 
X x3{1,2};

struct D : X 
{
  D(int x, int y) : X{x,y} { }; // Use in parent initialization
};

struct S 
{
  int a[3];
  S(int x, int y, int z) : a{x,y,z} { }; // solution to old problem
};
```

+++
#### Spot the bug !
<img src="media/bug/bug1.jpg" width="75%">

+++
#### Constructors and uniform initialization

```c++
std::vector<int> vec1();
vec1.push_back(3); 
std::cout << vec1.size() << std::endl;  // Output ?

std::vector<int> vec2(3);
std::cout << vec2.size() << std::endl;  // Output ?

std::vector<int> vec3(3, 3);
std::cout << vec3.size() << std::endl;  // Output ?

std::vector<int> vec4{ 3 };
std::cout << vec4.size() << std::endl;  // Output ?

std::vector<int> vec5{ 3, 3 };
std::cout << vec5.size() << std::endl;  // Output ?
```

+++
#### Constructors and uniform initialization

```c++
std::vector<int> vec1;
vec1.push_back(3); 
std::cout << vec1.size() << std::endl; // 1 - {3}

std::vector<int> vec2(3);
std::cout << vec2.size() << std::endl; // 3 - {0,0,0}

std::vector<int> vec3(3, 3);
std::cout << vec3.size() << std::endl; // 3 - {3,3,3}

std::vector<int> vec4{ 3 };
std::cout << vec4.size() << std::endl; // 1 - {3}

std::vector<int> vec5{ 3, 3 };
std::cout << vec5.size() << std::endl; // 2 - {3,3}
```

+++
#### Conclusion ?

* Initializer lists are useful for containers
* Stricter for certain implicit conversions
* Use initialization with ( ) to explicitly call a constructor that
is otherwise hidden by an initializer_list constructor.
* Curly braces -> initialization
* Normal braces -> function calls