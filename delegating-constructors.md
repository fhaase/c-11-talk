#### Delegating constructors

+++
#### Cpp98 Constructors

* If you want two constructors to do the same thing, repeat yourself or call "an init() function."

```c++
class Class_C 
{
public:
  Class_C( int x) { validate(x); }
  Class_C() { validate(42); }
  Class_C( std::string s) { int x = s.length(); validate(x); }

private:
  void validate(int x) { (0<x && x<=20) ? a = x : a = 0; }
  int a;
};
```

+++
#### Cpp11 Delegating constructors keyword

* Directly call the other constructor

```c++
class Class_C 
{
public:
  Class_C(int x) { (0<x && x<=20) ? a = x : a = 0; }
  Class_C() : Class_C{42} { }
  Class_C( std::string s) : Class_C{ s.length() } { }
private:
  int a;
};
``` 