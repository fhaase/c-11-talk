#### Container improvements

+++ 

```c++
// Vector initialization
std::vector<int> fibVec = { 1, 1, 2, 3, 5, 8, 13};

// Map initialization
std::map<std::string, int> ageMap = { { "Max", 33}, {"Fred", 42 }, {"Sam", 51} };

// Iterate over vec, find element with value 5 and print position in vector
std:vector<int>::iterator iVec;
for ( ; iVec != fibVec.end(); ++iVec  )
{
  unsigned int position = iVec - fibVec.begin();
  if ( *iVec == 5 )
  {
    std::cout << "Value 5 is at position " << position << std::endl
  }
}
```

@[1-2]
@[4-5]
@[6-16]