#include <boost/foreach.hpp>

std::vector<int> fibVec = { 1, 1, 2, 3, 5, 8, 13};

BOOST_FOREACH( int i , fibVec )
{
  std::cout << i << std::endl; 
}

BOOST_FOREACH( int& i , fibVec )
{
  i += 1; 
}

BOOST_FOREACH( const LotsOfData_C& largeObject , largeVec )
{
  largeObject.computeSth();
}