const enum ReturnValue_E
{
  RV_OK, RV_INVALID_ARGUMENT, RV_NOT_SUPPORTED,
  RV_FAILED, RV_NOT_REGISTERED
}

ReturnValue_E rv = RV_OK;
switch( rv )
{
  case RV_OK  : 
    std::cout << "OK !" << std::endl; break;
  default :
    std::cout << "NOT OK !" << std::endl; break;
}

int interesting = RV_OK;
bool doesThisMakeAnySense = RV_FAILED;