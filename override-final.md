#### Explicit overrides and final

+++
#### Cpp98 overrides

```c++
struct Base {
  virtual void f();
  virtual void g() const;
  virtual void h(char);
  void k();	// not virtual
};

struct Derived : Base {
  void f(); // overrides Base::f()
  void g(); // doesn't override Base::g() (wrong type)
  virtual void h(char); // overrides Base::h()
  void k(); // doesn't override Base::k() (Base::k() is not virtual)
};
```

- Did the programmer mean to override Base::g()? 
- Did the programming mean to override Base::h(char)?
- Did the programmer mean to override Base::k()? 


+++
#### Cpp11 -> **override** keyword

```c++
struct Base {
  virtual void f();
  virtual void g() const;
  virtual void h(char);
  void k(); // not virtual
};

struct Derived : Base {
  void f() override; // OK: overrides Base::f()
  void g() override; // error: wrong type
  virtual void h(char); // overrides Base::h(); likely warning
  void k() override; // error: Base::k() is not virtual
};
```

+++
#### Cpp11 -> **final** keyword

```c++
struct Base {
  virtual void f() const final; // do not override
  virtual void g();
};

struct Derived : Base {
  void f() const; // error: Derived::f attempts to override final Base::f
  void g(); // OK
};
```

+++
#### Conclusion

* Use explicit specifiers to express your intent
* Avoid accidentially (not) overriding 