const enum class ReturnValue_E
{
  OK, INVALID_ARGUMENT, NOT_SUPPORTED,
  FAILED, NOT_REGISTERED
}

ReturnValue_E rv = ReturnValue_E::OK;
switch( rv )
{
  case ReturnValue_E::OK :
    std::cout << "OK !" << std::endl; break;
  default :
    std::cout << "NOT OK !" << std::endl; break;
}

int test = ReturnValue_E::OK; // Fails to compile
bool doesThisMakeAnySense = ReturnValue_E::FAILED; // Fails to compile